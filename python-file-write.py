import os
import pathlib

current_dir = pathlib.Path(__file__).parent.absolute()

file_name = os.path.join(current_dir, "num.py")

with open(file_name, 'w') as f:
    counter = 1
    while counter < 2000:
        f.write("Line number: %d\n" % counter)
        counter += 1
